﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
    class GameFlow
    {
        Utilities util = new Utilities();
        Player p1 = new Player();
        Player p2 = new Player();
        Player p3 = new PLayer();
        
        Player currentPlayer;
        Player opponent;
        private string quit = "Y";

        public void StartGame()
        {
            while (quit == "Y")
            {
                util.StartScreen();
                do
                {
                Console.WriteLine("Player One, please enter your name: ");
                p1.pName = Console.ReadLine();
                } while (p1.pName.Length < 1);
                
                Console.Clear();

                do
                {
                    Console.WriteLine("Player Two, please enter your name: ");
                    p2.pName = Console.ReadLine();
                } while (p2.pName.Length < 1);
               
                 do
                {
                    Console.WriteLine("Player Three, please enter your name: ");
                    p2.pName = Console.ReadLine();
                } while (p3.pName.Length < 1);
                Console.Clear();

                util.PlainBoard();
                Console.WriteLine("{0}, place your ships...", p1.pName);
                Setupship(p1);
                Console.Clear();

                util.PlainBoard();
                Console.WriteLine("{0}, place your ships...", p2.pName);
                Setupship(p2);
                Console.Clear();

                Turns();

                Console.WriteLine("Would you like to play again?  Y/N");
                quit = Console.ReadLine();
            }
        }

        private void Setupship(Player p)
        {
            int userChoiceY;
            int i = 0;
            string Input;
            int userChoiceDir;
            bool parsed;

            while (i < 5)
            {
                do
                {
                    Console.WriteLine("Enter starting location for your {0}:  \nExample: (A1)", (ShipType)i);
                    Input = Console.ReadLine();
                } while (!util.StringChecker(Input) || Input == "" );

                int userChoiceX = util.CoordinateTranslator(Input.Substring(0, 1)); //use translate coord here

                parsed = Int32.TryParse(Input.Substring(1), out userChoiceY );

                do
                {
                    Console.WriteLine("Which direction would you like to place it? (Up[0], Down[1], Left[2], Right[3])... ");
                    parsed = Int32.TryParse(Console.ReadLine(), out userChoiceDir);
                } while (!util.DirectionChecker(userChoiceDir, parsed));

                PlaceShipRequest request = new PlaceShipRequest()
                {
                    Coordinate = new Coordinate(userChoiceX, userChoiceY),
                    Direction = (ShipDirection) userChoiceDir,
                    ShipType = (ShipType) i
                };
                
                Enum response =  p.PlayerBoard.PlaceShip(request);
                //check for shipplacemnt response. 1 of 3 values. 
                if (response.Equals(ShipPlacement.NotEnoughSpace) || response.Equals(ShipPlacement.Overlap))
                {
                    Console.Clear();
                    util.PlainBoard();
                    Console.WriteLine("{0} is an invalid placement. Reason: {1}. Try again.\n", Input, response);
                }
              
                else
                {
                    i++;
                    Console.Clear();
                    util.PlainBoard();
                }

            }
        }

        public void Turns()
        {
            currentPlayer = p1;
            opponent = p2;
            int userChoiceY;
            bool keepPlaying = true;
            Player temp = new Player();
            string Input;

            Console.Clear();
            util.PlainBoard();

            while (keepPlaying)
            {
                // II. Ask for player coords

                FireShotResponse shotResponse = new FireShotResponse();

                do // check for duplicates
                {
                    do // valiate input
                    {
                        Console.WriteLine("{0}, take aim and fire!  Example: A1, B7", currentPlayer.pName);
                        Input = Console.ReadLine();
                    } while (!util.StringChecker(Input));

                    int userChoiceX = util.CoordinateTranslator(Input.Substring(0, 1)); //use translate coord here
                    bool yTest = int.TryParse(Input.Substring(1), out userChoiceY);

                    // III. Feed coordinates to fireshot and return shot result. 
                    Coordinate shot = new Coordinate(userChoiceX, userChoiceY);
                    shotResponse = opponent.PlayerBoard.FireShot(shot);
                    Console.Clear();

                    Console.WriteLine("{0}, the status of your shot at {1} was... {2}", currentPlayer.pName, Input,
                        shotResponse.ShotStatus);

                } while (shotResponse.ShotStatus == ShotStatus.Duplicate);
                Console.WriteLine("  --A--B--C--D--E--F--G--H--I--J-+");
                for (int i = 1; i < 11; i++)
                {
                    if (i < 10)
                        Console.Write(" ");
                    Console.Write(i + "|");
                    for (int j = 1; j < 11; j++)
                    {
                        var myCoord = new Coordinate(j, i);

                        if (currentPlayer.PlayerBoard.ShotHistory.ContainsKey(myCoord))
                        {
                            if (currentPlayer.PlayerBoard.ShotHistory[myCoord] == ShotHistory.Hit)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write(" H ");
                                Console.ResetColor();
                            }
                            else if (currentPlayer.PlayerBoard.ShotHistory[myCoord] == ShotHistory.Miss)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write(" M ");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            Console.Write(" ? ");
                        }
                    }
                    Console.Write("|\n");
                }
                Console.WriteLine("  -------------------------------+");

                if (shotResponse.ShotStatus == ShotStatus.Victory)
                {
                    Console.Clear();
                    Console.WriteLine("{0} wins!", currentPlayer.pName);
                    Console.ReadLine();
                    keepPlaying = false;
                }
                 
                //IV. Switch Players 
                          
                    temp = currentPlayer;
                    currentPlayer = opponent;
                    opponent = temp;
            }
        }

       
    }
}
