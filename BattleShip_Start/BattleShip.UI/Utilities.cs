﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Utilities
    {
        public void StartScreen()
        {
            string[] opening = new string[7];

            opening[0] = "XXXXX      X    XXXXXXX XXXXXXX X      XXXXXX XXXXXX X    X XXXXX XXXXX \n";
            opening[1] = "X    X     X    X  X  X X  X  X X      X      X      X    X   X   X    X\n";
            opening[2] = "XXXXX     X X      X       X    X      XXX    XXXXXX XXXXXX   X   X    X\n";
            opening[3] = "X    X   X   X     X       X    X      X           X X    X   X   XXXXX \n";
            opening[4] = "X     X  XXXXX     X       X    X      X           X X    X   X   X     \n";
            opening[5] = "XXXXXX  X     X    X       X    XXXXXX XXXXXX XXXXXX X    X XXXXX X     \n\n";
            opening[6] = "                        Press Enter To Start!";

            foreach (string i in opening)
            {
                Console.Write(i);
            }

            Console.ReadLine();
            Console.Clear();
        }

        public void PlainBoard()
        {
            Console.WriteLine("  --A--B--C--D--E--F--G--H--I--J-+");
            Console.WriteLine(" 1| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 2| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 3| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 4| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 5| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 6| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 7| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 8| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine(" 9| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine("10| ?  ?  ?  ?  ?  ?  ?  ?  ?  ? |");
            Console.WriteLine("  --------------------------------");
        }

        public bool StringChecker(string str)
        {
            int placeholder;

            if (str != "" && str.Length < 4 && Int32.TryParse(str.Substring(1), out placeholder) && char.IsLetter(str[0]))
                return true;

            Console.WriteLine("Please enter the input in the following format:  \"(letter)(number)\"");
            return false;
        }

        public bool DirectionChecker(int direction, bool isDirectionNumber)
        {
            if (direction >= 0 && direction <= 3 && isDirectionNumber)
                return true;

            Console.WriteLine("Captain, I need a number!");
            return false;
        }

        public int CoordinateTranslator(string let)
        {
            int x = 0;
            string xAxis = "xabcdefghij";
            for (int i = 0; i < 11; i++)
            {
                if (xAxis.Substring(i, 1) == let)
                    x = i;
            }

            return x;
        }

        /*  Original pie-in-the-sky if we had another week to mess with this board idea
        public void BoardGui(int x = 0, int y = 0, string shot = "")
        {
            string[,] coordsToBoard = new string[10, 10];

            if (shot == "Hit")
            {

                coordsToBoard[x, y] = ("H");
            }

            if (shot == "Miss")
            {
                coordsToBoard[x, y] = "M";
            }

            Console.WriteLine("  +-A-+-B-+-C-+-D-+-E-+-F-+-G-+-H-+-I-+-J-+");
            Console.WriteLine("1 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 0], coordsToBoard[1, 0], coordsToBoard[2, 0], coordsToBoard[3, 0], coordsToBoard[4, 0], coordsToBoard[5, 0], coordsToBoard[6, 0], coordsToBoard[7, 0], coordsToBoard[8, 0], coordsToBoard[9, 0]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("2 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 1], coordsToBoard[1, 1], coordsToBoard[2, 1], coordsToBoard[3, 1], coordsToBoard[4, 1], coordsToBoard[5, 1], coordsToBoard[6, 1], coordsToBoard[7, 1], coordsToBoard[8, 1], coordsToBoard[9, 1]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("3 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 2], coordsToBoard[1, 2], coordsToBoard[2, 2], coordsToBoard[3, 2], coordsToBoard[4, 2], coordsToBoard[5, 2], coordsToBoard[6, 2], coordsToBoard[7, 2], coordsToBoard[8, 2], coordsToBoard[9, 2]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("4 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 3], coordsToBoard[1, 3], coordsToBoard[2, 3], coordsToBoard[3, 3], coordsToBoard[4, 3], coordsToBoard[5, 3], coordsToBoard[6, 3], coordsToBoard[7, 3], coordsToBoard[8, 3], coordsToBoard[9, 3]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("5 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 4], coordsToBoard[1, 4], coordsToBoard[2, 4], coordsToBoard[3, 4], coordsToBoard[4, 4], coordsToBoard[5, 4], coordsToBoard[6, 4], coordsToBoard[7, 4], coordsToBoard[8, 4], coordsToBoard[9, 4]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("6 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 5], coordsToBoard[1, 5], coordsToBoard[2, 5], coordsToBoard[3, 5], coordsToBoard[4, 5], coordsToBoard[5, 5], coordsToBoard[6, 5], coordsToBoard[7, 5], coordsToBoard[8, 5], coordsToBoard[9, 5]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("7 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 6], coordsToBoard[1, 6], coordsToBoard[2, 6], coordsToBoard[3, 6], coordsToBoard[4, 6], coordsToBoard[5, 6], coordsToBoard[6, 6], coordsToBoard[7, 6], coordsToBoard[8, 6], coordsToBoard[9, 6]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("8 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 7], coordsToBoard[1, 7], coordsToBoard[2, 7], coordsToBoard[3, 7], coordsToBoard[4, 7], coordsToBoard[5, 7], coordsToBoard[6, 7], coordsToBoard[7, 7], coordsToBoard[8, 7], coordsToBoard[9, 7]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("9 |  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 8], coordsToBoard[1, 8], coordsToBoard[2, 8], coordsToBoard[3, 8], coordsToBoard[4, 8], coordsToBoard[5, 8], coordsToBoard[6, 8], coordsToBoard[7, 8], coordsToBoard[8, 8], coordsToBoard[9, 8]);
            Console.WriteLine("  +---------------------------------------+");
            Console.WriteLine("10|  {0} |  {1} |  {2} |  {3} |  {4} |  {5} |  {6} |  {7} |  {8} |  {9} |", coordsToBoard[0, 8], coordsToBoard[1, 8], coordsToBoard[2, 8], coordsToBoard[3, 8], coordsToBoard[4, 8], coordsToBoard[5, 8], coordsToBoard[6, 8], coordsToBoard[7, 8], coordsToBoard[8, 8], coordsToBoard[9, 8]);
            Console.WriteLine("  +---------------------------------------+");
        }
        */
    }
}
